(define-module (tests harness-intake)
  #:use-module (spec)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients)
  #:use-module (ynm-core use-cases intake)
  #:use-module (ynm-core adapters))

(install-spec-runner-repl)

(define CALNUT_TABLE_NO_ALIM "tests/test-files/calnut-table-no-alim.csv")
(define CALNUT_TABLE_ONE_ALIM_1 "tests/test-files/calnut-table-one-alim.csv")
(define CALNUT_TABLE_ONE_ALIM_2 "tests/test-files/calnut-table-one-alim-bis.csv")
(define CALNUT_TABLE_TWO_ALIMS "tests/test-files/calnut-table-two-alims.csv")

(define-public (run-tests-intakes)
  (describe "intakes"
    (it "from nothing"
      (should=
	  (make-intake 0 0)
	(intake '())))
    (it "from one single ingredient with reference quantity"
      (should=
	  (make-intake 682 162)
	(intake
	 (list
	  (cons
	   (car
	    (ingredients
	     (build-calnut-base CALNUT_TABLE_TWO_ALIMS)
	     #:label "Abat, cuit (aliment moyen)")) 100)))))
    (it "from one single ingredient with reference quantity bis"
      (should=
	  (make-intake 1640 395)
	(intake
	 (list
	  (cons
	   (car
	    (ingredients
	     (build-calnut-base CALNUT_TABLE_TWO_ALIMS)
	     #:label "Abondance"))
	   100)))))
    (it "from one single ingredient with arbitrary quantity"
      (should=
	  (make-intake 3280 790)
	(intake
	 (list
	  (cons
	   (car
	    (ingredients
	     (build-calnut-base CALNUT_TABLE_TWO_ALIMS)
	     #:label "Abondance"))
	   200)))))
    (it "from two ingredients"
      (should=
	  (make-intake
	   6284
	   1509)
	(intake
	 (list
	  (cons
	   (car
	    (ingredients
	     (build-calnut-base CALNUT_TABLE_TWO_ALIMS)
	     #:label "Abat, cuit (aliment moyen)"))
	   200)
	  (cons
	   (car
	    (ingredients
	     (build-calnut-base CALNUT_TABLE_TWO_ALIMS)
	     #:label "Abondance"))
	   300)))))))
