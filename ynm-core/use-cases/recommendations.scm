(define-module (ynm-core use-cases recommendations)
  #:use-module (ynm-core entities)
  #:use-module (ynm-core use-cases ingredients))

(define-public (recommendations balance food-base)
  (cond ((zero? (energy-kcal balance)) '())
	((negative? (energy-kcal balance)) (richest-in-energy food-base))
	((positive? (energy-kcal balance)) (poorest-in-energy food-base))))

(define (richest-in-energy food-base)
  (car
   (sort
    (ingredients food-base)
    (lambda (ingr1 ingr2)
      (>
       (energy-kcal (food-intake ingr1))
       (energy-kcal (food-intake ingr2)))))))

(define (poorest-in-energy food-base)
  (car
   (sort
    (ingredients food-base)
    (lambda (ingr1 ingr2)
      (<
       (energy-kcal (food-intake ingr1))
       (energy-kcal (food-intake ingr2)))))))
