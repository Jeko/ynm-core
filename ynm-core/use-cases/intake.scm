(define-module (ynm-core use-cases intake)
  #:use-module (srfi srfi-1)
  #:use-module (ynm-core entities))

(define-public (intake meal)
  (if (null? meal)
      INTAKE_NULL
      (reduce
       intake-add
       INTAKE_NULL
       (map
	intake-from-meal-entry
	meal))))

(define (intake-from-meal-entry entry)
  (make-intake
   (* (/ (cdr entry) 100) (energy-kj (food-intake (car entry))))
   (* (/ (cdr entry) 100) (energy-kcal (food-intake (car entry))))))
