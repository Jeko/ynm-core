(define-module (ynm-core use-cases balance)
  #:use-module (ynm-core entities))

(define-public (balance profile intake)
  (if (gender=? 'woman (profile-gender profile))
      (intake-soustract intake INTAKE_BNM_DAILY_WOMAN_30-39_58kg)
      (intake-soustract intake INTAKE_BNM_DAILY_MAN_30-39_69kg)))
