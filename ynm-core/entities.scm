(define-module (ynm-core entities)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-1)
  #:export (make-profile
	    profile-gender
	    
	    make-food
	    food-code
	    food-label
	    food-intake

	    make-intake
	    energy-kj
	    energy-kcal
	    ))

(define-immutable-record-type <profile>
  (make-profile gender age weight mass-index)
  profile?
  (gender profile-gender)
  (age profile-age)
  (weight profile-weight)
  (mass-index profile-mass-index))

(define-public gender=?
  (lambda (gender1 gender2)
    (eq? gender1 gender2)))

(define-immutable-record-type <food>
  (make-food alim_code food_label hypoth intake)
  food?
  (alim_code food-code)
  (food_label food-label)
  (hypoth food-hypothesis)
  (intake food-intake))

(define-immutable-record-type <intake>
  (make-intake nrj_kj nrj_kcal)
  intake?
  (nrj_kj energy-kj)
  (nrj_kcal energy-kcal) ;;eau_g,sel_g,sodium_mg,magnesium_mg,phosphore_mg,potassium_mg,calcium_mg,manganese_mg,fer_mg,cuivre_mg,zinc_mg,selenium_mcg,iode_mcg,proteines_g,glucides_g,sucres_g,fructose_g,galactose_g,lactose_g,glucose_g,maltose_g,saccharose_g,amidon_g,polyols_g,fibres_g,lipides_g,ags_g,agmi_g,agpi_g,ag_04_0_g,ag_06_0_g,ag_08_0_g,ag_10_0_g,ag_12_0_g,ag_14_0_g,ag_16_0_g,ag_18_0_g,ag_18_1_ole_g,ag_18_2_lino_g,ag_18_3_a_lino_g,ag_20_4_ara_g,ag_20_5_epa_g,ag_20_6_dha_g,retinol_mcg,beta_carotene_mcg,vitamine_d_mcg,vitamine_e_mg,vitamine_k1_mcg,vitamine_k2_mcg,vitamine_c_mg,vitamine_b1_mg,vitamine_b2_mg,vitamine_b3_mg,vitamine_b5_mg,vitamine_b6_mg,vitamine_b12_mcg,vitamine_b9_mcg,alcool_g,acides_organiques_g,cholesterol_mg,alim_grp_code,alim_grp_nom_fr,alim_ssgrp_code,alim_ssgrp_nom_fr,alim_ssssgrp_code,alim_ssssgrp_nom_fr
  )

(define-public INTAKE_BNM_DAILY_WOMAN_30-39_58kg
  (make-intake 8598 2055))

(define-public INTAKE_BNM_DAILY_MAN_30-39_69kg
  (make-intake 10770 2574))

(define-public INTAKE_NULL
  (make-intake 0 0))

(define-public intake=
  (lambda (intake1 intake2)
    (and (= (energy-kj intake1) (energy-kj intake2))
	 (= (energy-kcal intake1) (energy-kcal intake2)))))

(define-public intake-soustract
  (lambda (intake1 intake2)
    (make-intake
     (- (energy-kj intake1) (energy-kj intake2))
     (- (energy-kcal intake1) (energy-kcal intake2)))))

(define-public intake-add
  (lambda (intake1 intake2)
    (make-intake
     (+ (energy-kj intake1) (energy-kj intake2))
     (+ (energy-kcal intake1) (energy-kcal intake2)))))
